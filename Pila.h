#include <cstddef>
#ifndef PILA_H
#define PILA_H

class Pila {
    private:
		int max = 0;
		int tope = 0;
		int *datos = 0;
		bool band = NULL;

    public:
        /* constructores */
        //Pila ();
        Pila (int max, int tope, int *datos, bool band);
        
        /* métodos get and set */
        int get_max();
        int get_tope();
        int get_datos();
        bool get_band();
        void set_max(int max);
        void set_tope(int tope);
        void set_datos(int *datos);
        void set_band(bool band);
};
#endif
