#include <iostream>
#include <cstddef>
using namespace std;
#include "Pila.h"

/* constructores 
Pila::Pila() {
	int max = 0;
	int tope = 0;
	int *datos = 0;
	bool band = NULL;
}*/

Pila::Pila (int max, int tope, int *dato, bool band) {
	this->max = max;
	this->tope = tope;
	this->dato = int *dato;
	this->band = band;
}

/* métodos get and set */
int Pila::get_max() {
	return this->max;
}

int Pila::get_tope() {
	return this->tope;
}
        
int Pila::get_dato() {
	return this->int *dato;
}

bool Pila::get_band() {
	return this->band;
}

void Pila::set_max(int max) {
    this->max = max;
}
        
void Pila::set_tope(int tope) {
    this->tope = tope;
}
        
void Pila::set_dato(int *dato) {
    this->*dato = *dato;
}

void Pila::set_band(bool band) {
    this->band = band;
}

void Pila::pila_vacia(int tope, bool band){
	if (tope == -1){
		band = true; //pila vacia
		}
	else {
		band = false; //pila no vacia
		}
}

void Pila::pila_llena(int tope, int max, bool band){
	if (tope == max){
		band = true; //pila llena
		}
	else {
		band = false; //pila no llena
		}
}

void Pila::Push(Dato *pila, int n){
	Dato *nuevo_dato = new Dato();
	nuevo_dato->dato = n;
	nuevo_dato->siguiente = pila;
	pila = nuevo_dato; 
}

void Pila::Pop(Dato *pila, int n){
	Dato *aux = pila;
	n = aux->dato;
	pila = aux->siguiente;
	delete aux;
	}
