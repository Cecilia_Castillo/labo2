#include <iostream>
#include <stdlib.h>
#include <cstddef>
using namespace std;
#include "Pila.h"

struct Dato{
	int dato;
	Dato *siguiente;
};

void Push(Dato *pila, int n){
	Dato *nuevo_dato = new Dato();
	nuevo_dato->dato = n;
	nuevo_dato->siguiente = pila;
	pila = nuevo_dato; 
	}

void Pop(Dato *pila, int n){
	Dato *aux = pila;
	n = aux->dato;
	pila = aux->siguiente;
	delete aux;
	}

int main(int argc, char **argv){
	
	Dato *pila = NULL;
	int dato;
	int max;
	
	//cout << "Tamaño  maximo de pila: " << endl;
	//cin >> max;
	cout << "Escriba un nùmero: ";
	cin >> dato;
	Push(pila, dato);
	
	cout << "Sacar elemento de la pila: ";
	while(pila != NULL){
		Pop(pila, dato);
		if(pila !=NULL){
			cout << dato << " , ";
			}
		else{
			cout << dato << ".";
			}
		}
	
	return 0;
}

